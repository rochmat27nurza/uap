package Contest;

class CAT1 extends TiketKonser {
    //Do your magic here...
    //memanggil konstruktor class CAT1 dengan parent TiketKonser dan ionisiasi nama jenis tiker CAT1
    public CAT1() {
        super("CAT1");
    }

    // untuk mengembalikan harga dari tiket class CAT1
    @Override
    public int getHargaTiket() {
        return 1000000;
    }
}