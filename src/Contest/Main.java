/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Do your magic here...
        // tampilan yang didapatkan oleh user saat akan memesan tiket
        try {
        Scanner scanner = new Scanner(System.in);

        // Meminta input nama pemesan
        System.out.println("Selamat datang di Pemesanan Tiket Coldplay!");
        System.out.print("Masukkan nama pemesan: ");
        String namaPemesan = scanner.nextLine();

        // Menampilkan pilihan tiket kepada pengguna
        System.out.println("Pilihan tiket:");
        System.out.println("1. CAT8");
        System.out.println("2. CAT1");
        System.out.println("3. FESTIVAL");
        System.out.println("4. VIP");
        System.out.println("5. UNLIMITED EXPERIENCE");
        System.out.print("Masukkan nomor tiket yang diinginkan: ");
        int nomorTiket = scanner.nextInt();

        // Mendapatkan objek tiket berdasarkan nomor tiket yang diinput
        TiketKonser tiket = null;
        // berguna untuk menu pilihan tiket sesuai dengan keinginan user
        switch (nomorTiket) {
            case 1:
                tiket = new CAT8();
                break;
            case 2:
                tiket = new CAT1();
                break;
            case 3:
                tiket = new FESTIVAL();
                break;
            case 4:
                tiket = new VIP();
                break;
            case 5:
                tiket = new VVIP();
                break;
            default:
                System.out.println("Nomor tiket tidak valid.");
                System.exit(0);
        }

        // Mendapatkan kode pesanan dan tanggal pesanan
        String kodePesanan = generateKodeBooking();
        String tanggalPesanan = getCurrentDate();

        // Menampilkan detail pesanan
        System.out.println("\n-----Detail Pemesanan-----");
        System.out.println("Nama Pemesan : " + namaPemesan);
        System.out.println("Kode Pesanan : " + kodePesanan);
        System.out.println("Tanggal Pesanan : " + tanggalPesanan);
        System.out.println("Nama Tiket : " + tiket.getNamaTiket());
        System.out.println("Total Harga : " + tiket.getHargaTiket());

        scanner.close();
        } 
        // untu menangkap kesalahan input pengguna 
        catch (Exception e) {
        System.out.println("Mohon maaf input anda salah");
        }
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}