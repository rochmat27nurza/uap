package Contest;

class PemesananTiket {
    //Do your magic here...
    private String namaPemesan;
    private String kodePesanan;
    private String tanggalPesanan;
    private TiketKonser tiket;

    // konstruktor pemesanan tiket dengan parameter  
    public PemesananTiket(String namaPemesan, String kodePesanan, String tanggalPesanan, TiketKonser tiket) {
        this.namaPemesan = namaPemesan;
        this.kodePesanan = kodePesanan;
        this.tanggalPesanan = tanggalPesanan;
        this.tiket = tiket;
    }

    public String getNamaPemesan() {
        return namaPemesan;
    }

    public String getKodePesanan() {
        return kodePesanan;
    }

    public String getTanggalPesanan() {
        return tanggalPesanan;
    }

    public TiketKonser getTiket() {
        return tiket;
    }
}
