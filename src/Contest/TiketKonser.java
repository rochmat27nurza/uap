package Contest;

abstract class TiketKonser implements HargaTiket {
    // Do your magic here...
    private String namaTiket;
    // konstruktor dari TikerKonser
    public TiketKonser(String namaTiket) {
        this.namaTiket = namaTiket;
    }

    // method untuk mengambil nama dari tiket konser
    public String getNamaTiket() {
        return namaTiket;
    }
}