package Contest;
// merupakan class anak dari TiketKoser
class VIP extends TiketKonser {
    // Do your magic here...
    // inisiasi nama tiket VIP
    // memanggil Konstrukor class VIP
    public VIP() {
        super("VIP");
    }

    // menginisiasi harga tiket VIP dan mereturn nilainya
    @Override
    public int getHargaTiket() {
        return 2000000;
    }
}