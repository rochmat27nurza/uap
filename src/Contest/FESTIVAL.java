package Contest;

class FESTIVAL extends TiketKonser {
    //Do your magic here...
    // memanggil konstruktor class FESTIVAL dan inisiasi jenis tiket Festival
    public FESTIVAL() {
        super("FESTIVAL");
    }

    // mendapatkan harga tiket FESTIVAL dan mengembalikan nilainya
    @Override
    public int getHargaTiket() {
        return 1500000;
    }
}