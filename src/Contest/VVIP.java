package Contest;
// merupakan child class dari TiketKonser
class VVIP extends TiketKonser {
    // Do your magic here...
    // memanggil konstruktor class VVIP
    public VVIP() {
        super("UNLIMITED EXPERIENCE");
    }

    @Override
    public int getHargaTiket() {
        return 2500000;
    }
}