package Contest;

class CAT8 extends TiketKonser {
    //Do your magic here...
    //memanggil konstruktor class CAT8 dengan parent TiketKonser dan inisiasi nama jenis tiker CAT8
    public CAT8() {
        super("CAT8");
    }

    // mengembalikan harga tiket CAT8 dengan parent TiketKonser
    @Override
    public int getHargaTiket() {
        return 500000;
    }
}

